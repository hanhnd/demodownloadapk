package com.vmodev.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.vtop.iwinonline2.GameUrl;
import com.vtop.iwinonline2.MainActivity;
import com.vtop.iwinonline2.R;

public class SimpleImageFragment extends Fragment {
	public static final String PARAM_GAME_URL = "IMAGE_id";
	private GameUrl game;

	public static SimpleImageFragment newInstant(GameUrl game) {
		SimpleImageFragment fragment = new SimpleImageFragment();
		Bundle args = new Bundle();
		args.putSerializable(SimpleImageFragment.PARAM_GAME_URL, game);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		game = (GameUrl) getArguments().getSerializable(PARAM_GAME_URL);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ImageView imageView = new ImageView(getActivity());
		LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);
		imageView.setLayoutParams(params);
		imageView.setBackgroundColor(getResources().getColor(R.color.white));
		imageView.setScaleType(ScaleType.FIT_CENTER);
		imageView.setImageResource(game.getResId());
		imageView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				MainActivity mainActivity = (MainActivity) getActivity();
				mainActivity.downloadApk(game.getUrl(), game.getFilename()
						+ ".apk");
			}
		});
		return imageView;
	}
}
