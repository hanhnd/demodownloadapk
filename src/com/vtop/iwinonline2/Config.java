package com.vtop.iwinonline2;

public class Config {
	public static final GameUrl[] gameUrls = new GameUrl[] {
			new GameUrl(R.drawable.image2,
					"http://vmoapp.mtai.me/edl/c383418/92400/ManhThu_V275.apk",
					"ManhThu_V275"),
			new GameUrl(R.drawable.image3,
					"http://vmoapp.mtai.me/edl/c234947/70354/NLTB126.apk",
					"NLTB126"),
			new GameUrl(
					R.drawable.image4,
					"http://vmoapp.mtai.me/edl/c376349/87509/Dai_Tuong_2412.apk",
					"Dai_Tuong_2412") };

	public static final Game[] games = new Game[] {
			new Game(
					R.drawable.icon_app_vegas,
					"Vegas Online – MXH Facebook Đánh Bài",
					"vegas",
					"Vegas Online MXH Facebook thu nhỏ kết hợp chơi 12 game bài Đẳng Cấp",
					"1.000", 4f,
					"http://taifile.mobi/9306-than-bai-18/Vegas109_59/apk"),
			new Game(
					R.drawable.icon_app_bai_ma_thuat,
					"Bàn tay ma thuật",
					"bai_ma_thuat",
					"\"Đỏ mắt\" với những màn lột áo khoe ngực của mỹ nhân bằng chính bàn tay ma thuật của bạn.",
					"1.500", 4f,
					"http://taifile.mobi/9306-ban-tay-ma-thuat-232/apk/apk"),
			new Game(
					R.drawable.icon_app_ibet88,
					"iBet88 311 - Game Bài Đẳng Cấp",
					"ibet88",
					"Game Bài hỗ trợ tất các dòng máy mobile, đấu trường, luật chơi, thách thức mới, nghẹt thở trong từng ván bài....",
					"2.000", 4f,
					"http://taifile.mobi/9306-ibet-183/ibet88_3.2.13_6380/apk") };
}
