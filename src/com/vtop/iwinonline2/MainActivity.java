package com.vtop.iwinonline2;

import java.io.File;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class MainActivity extends FragmentActivity implements
		OnItemClickListener {

	private final BroadcastReceiver downloadCompleteReceiver = new DonwloadCompleteReceiver();

	private ProgressDialog progressDialog;

	private ViewPager viewPager;

	private Handler handler;

	private View btnDownloadIonline;
	private View btnDownloadIwin;
	private View progressBarIonline;
	private View progressBarIwin;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		registerReceiver(downloadCompleteReceiver, new IntentFilter(
				DownloadManager.ACTION_DOWNLOAD_COMPLETE));
		viewPager = (ViewPager) findViewById(R.id.viewpager);
		ImageAdapter adapter = new ImageAdapter(getSupportFragmentManager(),
				Config.gameUrls);
		viewPager.setAdapter(adapter);

		ListView listview = (ListView) findViewById(R.id.listview);
		listview.setOnItemClickListener(this);

		GameAdapter gameAdapter = new GameAdapter(this, R.layout.item_app,
				Config.games);
		listview.setAdapter(gameAdapter);

		progressDialog = new ProgressDialog(this);
		progressDialog.setTitle(R.string.loading);
		progressDialog.setMessage(getString(R.string.please_wait));
		progressDialog.setCancelable(false);
		handler = new PageHandler();
		handler.sendMessageDelayed(new Message(), 3000);

		btnDownloadIonline = findViewById(R.id.btnDownloadIonline);
		btnDownloadIwin = findViewById(R.id.btnDownloadIwin);
		progressBarIonline = findViewById(R.id.progressBarIonline);
		progressBarIwin = findViewById(R.id.progressBarIwin);
	}

	private void switchView(View show, View hide) {
		show.setVisibility(View.VISIBLE);
		hide.setVisibility(View.GONE);
	}

	private class PageHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			int index = viewPager.getCurrentItem() + 1;
			if (index >= viewPager.getChildCount()) {
				index = 0;
			}
			viewPager.setCurrentItem(index);
			handler.sendMessageDelayed(new Message(), 3000);
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		handler = null;
		unregisterReceiver(downloadCompleteReceiver);
	}

	public void onClickIonline(View view) {
		downloadApk(getString(R.string.url_ionline),
				getString(R.string.name_file_ionline));
		switchView(progressBarIonline, btnDownloadIonline);
	}

	public void onClickIwin(View view) {
		downloadApk(getString(R.string.url_iwin),
				getString(R.string.name_file_iwin));
		switchView(progressBarIwin, btnDownloadIwin);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		progressDialog.show();
		Game game = (Game) parent.getItemAtPosition(position);
		downloadApk(game.getUrl(), game.getFileName() + ".apk");
	}

	public void downloadApk(String url, String filename) {
		Uri uri = Uri.parse(url);
		DownloadManager.Request r = new DownloadManager.Request(uri);

		// This put the download in the same Download dir the browser uses
		r.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,
				filename);

		// When downloading music and videos they will be listed in the player
		// (Seems to be available since Honeycomb only)
		r.allowScanningByMediaScanner();

		// Notify user when download is completed
		// (Seems to be available since Honeycomb only)
		r.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

		// Start download
		DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
		dm.enqueue(r);
	}

	private class DonwloadCompleteReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			progressDialog.dismiss();
			Bundle extras = intent.getExtras();
			DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
			DownloadManager.Query q = new DownloadManager.Query();
			q.setFilterById(extras.getLong(DownloadManager.EXTRA_DOWNLOAD_ID));
			Cursor c = dm.query(q);
			String namefile = "xxx";
			if (c.moveToFirst()) {
				int status = c.getInt(c
						.getColumnIndex(DownloadManager.COLUMN_STATUS));
				if (status == DownloadManager.STATUS_SUCCESSFUL) {
					// process download
					namefile = c.getString(c
							.getColumnIndex(DownloadManager.COLUMN_TITLE));

					if (namefile.contains("IOnline")) {
						switchView(btnDownloadIonline, progressBarIonline);
					} else if (namefile.contains("IWin")) {
						switchView(btnDownloadIwin, progressBarIwin);
					}
					// get other required data by changing the constant passed
					// to getColumnIndex
					Intent newintent = new Intent(Intent.ACTION_VIEW);
					newintent.setDataAndType(Uri.fromFile(new File(Environment
							.getExternalStorageDirectory()
							+ "/download/"
							+ namefile)),
							"application/vnd.android.package-archive");
					newintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(newintent);
				}
			}
		}

	}
}
