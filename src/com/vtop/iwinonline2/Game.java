package com.vtop.iwinonline2;

public class Game {
	private int iconId;
	private String name;
	private String fileName;
	private String description;
	private String downloadCounter;
	private float star;
	private String url;

	public Game(int iconId, String name, String fileName, String description,
			String downloadCounter, float star, String url) {
		this.iconId = iconId;
		this.name = name;
		this.fileName = fileName;
		this.description = description;
		this.downloadCounter = downloadCounter;
		this.star = star;
		this.setUrl(url);
	}

	public int getIconId() {
		return iconId;
	}

	public void setIconId(int iconId) {
		this.iconId = iconId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDownloadCounter() {
		return downloadCounter;
	}

	public void setDownloadCounter(String downloadCounter) {
		this.downloadCounter = downloadCounter;
	}

	public float getStar() {
		return star;
	}

	public void setStar(float star) {
		this.star = star;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
