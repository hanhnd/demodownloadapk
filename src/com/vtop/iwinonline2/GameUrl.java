package com.vtop.iwinonline2;

import java.io.Serializable;

public class GameUrl implements Serializable {
	private static final long serialVersionUID = -6734518325237848528L;
	private int resId;
	private String url;
	private String filename;

	public GameUrl(int resId, String url, String filename) {
		this.filename = filename;
		this.setResId(resId);
		this.setUrl(url);
	}

	public int getResId() {
		return resId;
	}

	public void setResId(int resId) {
		this.resId = resId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}
}
