package com.vtop.iwinonline2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class GameAdapter extends ArrayAdapter<Game> {

	private int resource;

	public GameAdapter(Context context, int resource, Game[] objects) {
		super(context, resource, objects);
		this.resource = resource;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(resource,
					null);
			holder = new ViewHolder();
			holder.imvIcon = (ImageView) convertView.findViewById(R.id.imvIcon);
			holder.tvName = (TextView) convertView.findViewById(R.id.tvName);
			holder.tvDownloadCounter = (TextView) convertView
					.findViewById(R.id.tvDownloadCounter);
			holder.tvDescription = (TextView) convertView
					.findViewById(R.id.tvDescription);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		if (position % 2 == 0) {
			convertView.setBackgroundColor(getContext().getResources()
					.getColor(R.color.white));
		} else {
			convertView.setBackgroundColor(getContext().getResources()
					.getColor(R.color.white2));
		}
		Game item = getItem(position);
		holder.imvIcon.setImageResource(item.getIconId());
		holder.tvName.setText(item.getName());
		String download = String.format(
				getContext().getString(R.string.download_counter),
				item.getDownloadCounter());
		holder.tvDownloadCounter.setText(download);
		holder.tvDescription.setText(item.getDescription());
		return convertView;
	}

	private static class ViewHolder {
		public ImageView imvIcon;
		public TextView tvName;
		public TextView tvDownloadCounter;
		public TextView tvDescription;
	}
}
