package com.vtop.iwinonline2;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.vmodev.fragment.SimpleImageFragment;

public class ImageAdapter extends FragmentPagerAdapter {
	private GameUrl[] images;

	public ImageAdapter(FragmentManager fm, GameUrl[] images) {
		super(fm);
		this.images = images;
	}

	@Override
	public Fragment getItem(int position) {
		SimpleImageFragment fragment = SimpleImageFragment
				.newInstant(images[position]);
		return fragment;
	}

	@Override
	public int getCount() {
		return images.length;
	}
}
